import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/vue";
import { defineComponent } from "vue";
import router from "@/router";
import { Plugins } from "@capacitor/core";
const { Storage } = Plugins;

export default defineComponent({
  name: "Product",
  data: () => ({
    product: {} as any,
    cachedProduct: {} as any,
    showAddPopup: false,
  }),
  computed: {
    totalWeight(): number {
      if (!this.product || !this.product.items) return 0;
      return this.product.items.reduce(function(
        a: number,
        b: { weight: number }
      ) {
        return a + +b.weight;
      },
      0);
    },
  },
  components: {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
  },
  setup() {
    return { router };
  },
  ionViewDidEnter() {
    const productString = router.currentRoute.value.params.product;
    if (productString == null) return;

    this.product = JSON.parse(productString.toString());
    this.cachedProduct = JSON.parse(JSON.stringify(this.product));
    console.log("this.product :>> ", this.product);
  },
  methods: {
    cloneItem(item: any) {
      this.product.items.push(item);
    },
    deleteItem(item: any) {
      this.product.items.splice(this.product.items.indexOf(item), 1);
    },
    refreshItems() {
      Storage.get({ key: "products" }).then((res) => {
        if (res.value != null) {
          const products = JSON.parse(res.value);
          const thisProduct = products.find(
            (p: { name: any }) => p.name == this.product.name
          );
          if (thisProduct) this.product = thisProduct;
        }
      });
    },
    addItem(itemInfo: any) {
      console.log('addItem!');
      this.showAddPopup = false;
      console.log('this.showAddPopup :>> ', this.showAddPopup);

      const item = {
        quantity: itemInfo.quantity,
        weight: itemInfo.weight
      }
      this.product.items.push(item);
    },
    saveChanges() {
      Storage.get({ key: "products" }).then((res) => {
        if (res.value != null) {
          const products = JSON.parse(res.value);

          const thisProduct = products.find(
            (p: { name: any }) => p.name == this.product.name
          );
          if (!thisProduct) return;

          products.splice(products.indexOf(thisProduct), 1);
          products.push(this.product);
          Storage.set({ key: "products", value: JSON.stringify(products) });
          this.cachedProduct = JSON.parse(JSON.stringify(this.product));
          this.refreshItems();
        }
      });
    },
  },
});
