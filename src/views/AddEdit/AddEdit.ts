import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/vue';
import { defineComponent } from 'vue';

export default defineComponent({
  name: 'AddEdit',
  props: ['products', 'editMode', 'product'],
  data: () => ({
    name: '',
    quantity: 1,
    weight: 0,
    totalWeight: 0,
    choosenColor: 'rgb(224, 255, 223)',
    colors: [],
    isProductPage: false,
    showNameLookup: false,
    lookupNameOptions: [] as any,
    lookupSelected: false,
  }),
  computed: {
    quantityValue(): number {
      return this.quantity;
    },
  },
  components: {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
  },
  mounted() {
    this.colors = require('../../data/Colors.json');
    if (this.colors && this.colors.length > 0)
      this.choosenColor = this.colors[0];

    this.setAllLookupNames();
    if (this.editMode) this.initProduct();
  },
  methods: {
    initProduct() {
      this.name = this.product.name;
      this.choosenColor = this.product.color;
      this.quantity = this.product.quantity;
      this.weight = this.product.weight;
    },
    setAllLookupNames() {
      console.log(this.products.length);
      if (this.products == null || this.products.length == 0) return;

      this.lookupNameOptions = this.products.map((p: { name: any }) => p.name);
      console.log('this.lookupNameOptions :>> ', this.lookupNameOptions);
    },
    close() {
      this.$emit('closeAddPopup');
    },
    add() {
      const productInfo = {
        name: this.name,
        color: this.choosenColor,
        quantity: this.quantity,
        weight: this.weight,
        weightTotal: this.totalWeight,
      };
      console.log('Add.ts emitting addProduct');
      this.$emit('addProduct', productInfo);
    },
    updateTotalWeight() {
      this.totalWeight = this.quantity * this.weight;
    },
    lookup4Name() {
      this.lookupSelected = false;
      this.choosenColor =
        this.colors && this.colors.length > 0 ? this.colors[0] : '';
      if (this.products == null || this.products.length == 0) return;
      this.showNameLookup = this.name.length > 0;
      const allNames = this.products.map((p: { name: any }) => p.name);
      /* eslint-disable */
      this.lookupNameOptions = allNames.filter((name: string) =>
        name.toLowerCase().startsWith(this.name.toLowerCase())
      );
      if (this.lookupNameOptions.length == 0) this.showNameLookup = false;
    },
    lookupSelect(name: string) {
      console.log('name :>> ', name);
      this.showNameLookup = false;
      this.name = name;
      const product = this.products.find(
        (p: { name: string }) => p.name == name
      );
      this.choosenColor = product.color;
      this.lookupSelected = true;
    },
  },
  watch: {
    quantity: function() {
      this.updateTotalWeight();
    },
  },
});
