import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/vue";
import { defineComponent } from "vue";
import router from "@/router";
import AddEdit from "../AddEdit/AddEdit.vue";
import { Plugins } from "@capacitor/core";
const { Storage } = Plugins;

export default defineComponent({
  name: "Home",
  data: () => ({
    products: [] as any,
    product: {} as any,
    cachedProducts: [] as any,
    showAddPopup: false,
    editMode: false,
  }),
  components: {
    IonContent,
    IonHeader,
    IonPage,
    IonTitle,
    IonToolbar,
    AddEdit,
  },
  setup() {
    return { router };
  },
  beforeRouteUpdate() {
    console.log("beforeRouteUpdate");
  },
  ionViewDidEnter() {
    this.fetchProducts();
  },
  methods: {
    fetchProducts() {
      Storage.get({ key: "products" }).then((res) => {
        if (res.value != null) {
          this.products = JSON.parse(res.value);
          this.cachedProducts = [...JSON.parse(JSON.stringify(this.products))];
          console.log("this.products :>> ", this.products);
        }
      });
    },
    deleteProduct(product: never) {
      this.products.splice(this.products.indexOf(product), 1);
    },
    getTotalQuantity(product: any) {
      if (!product || !product.items) return 0;
      return product.items.reduce(function(a: number, b: { quantity: number }) {
        return a + +b.quantity;
      }, 0);
    },
    getTotalWeight(product: any) {
      if (!product || !product.items) return 0;
      return product.items.reduce(function(a: number, b: { weight: number }) {
        return a + +b.weight;
      }, 0);
    },
    saveChanges() {
      Storage.set({ key: "products", value: JSON.stringify(this.products) });
      this.cachedProducts = [...JSON.parse(JSON.stringify(this.products))];
    },
    refreshProducts() {
      Storage.get({ key: "products" }).then((res) => {
        if (res.value != null) {
          this.products = JSON.parse(res.value);
        }
      });
    },
    addProduct(productInfo: any) {
      console.log("productInfo :>> ", productInfo);
      this.showAddPopup = false;
      console.log("this.showAddPopup :>> ", this.showAddPopup);

      const existingProduct = this.products.find(
        (p: { name: string }) => p.name === productInfo.name
      );
      const item = {
        quantity: productInfo.quantity,
        weight: productInfo.weight,
      };

      if (!existingProduct) {
        const product = {
          name: productInfo.name,
          quantity: productInfo.quantity,
          weight: productInfo.weight,
          color: productInfo.color,
          items: [item],
        };
        this.products.push(product);
        console.log("INFO: ex novo product :>> ", product);
        console.log("this.products :>> ", this.products);
        return;
      }

      this.products.splice(this.products.indexOf(existingProduct), 1);
      existingProduct.items.push(item);
      this.products.push(existingProduct);
      console.log("this.products :>> ", this.products);
      console.log("this.cachedProducts :>> ", this.cachedProducts);
    },
    viewProduct(product: any) {
      this.saveChanges();
      router.push({ name: 'Product', params: { product: JSON.stringify(product) }, });
    },
    editProduct(product: any) {
      this.saveChanges();
      this.product = product;
      this.editMode = true;
      this.showAddPopup = true;
    }
  },
});
